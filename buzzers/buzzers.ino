int team1 = 0;
int team2 = 0;
int team3 = 0;
int order = 0;
int team1buzz = 2;
int team2buzz = 3;
int team3buzz = 4;
int resetBuzz = 13;
int team1R = 7;
int team1G = 6;
int team2R = 9;
int team2G = 8;
int team3R = 11;
int team3G = 10;


void setup() {
  Serial.begin(9600);
}

void setTeamLedColor (int team, int red, int green) {  
  switch (team) {
    case 0:
      analogWrite(red, 255);
      analogWrite(green, 255);
      break;
    case 1:
      analogWrite(green, 0);
      break;
    case 2:
      analogWrite(red, 0);
      analogWrite(green, 0);
      break;
    case 3:
      analogWrite(red, 0);
      break;
  }
}

void loop() {
  
  if (digitalRead(resetBuzz) == HIGH) {
    Serial.println("Reset buzzer");
    team1 = 0;
    team2 = 0;
    team3 = 0;
    order = 0;
  }

  if (digitalRead(team1buzz) == HIGH) {
    if ( team1 == 0 ) {
      Serial.println("Tim 1 buzzer");
      order += 1;
      team1 = order;
    }
  }
  
  if (digitalRead(team2buzz) == HIGH) {
    if ( team2 == 0 ) {
      Serial.println("Tim 2 buzzer");
      order += 1;
      team2 = order;
    }
  }
  
  if (digitalRead(team3buzz) == HIGH) {
    if ( team3 == 0 ) {
      Serial.println("Tim 3 buzzer");
      order += 1;
      team3 = order;
    }
  }
  
  setTeamLedColor(team1, team1R, team1G);
  setTeamLedColor(team2, team2R, team2G);
  setTeamLedColor(team3, team3R, team3G);
  
}
